const app = require('express')()
const server = require('http').Server(app)
const uuid = require('uuid')
const bodyParser = require('body-parser')
const { isObject, sortBy, last, get } = require('lodash')
const EVT = require('evtjs')
const util = require('./util')

// data
const shopsData = require('./data/shops')
const appointmentsData = require('./data/appointments')

server.listen(8080)
console.log('Started on port', 8080)

app.use(bodyParser.json())

// helpers
const sendData = (res, data) => res.send(JSON.stringify(data))
const toArray = data => (Array.isArray(data) ? data : [data])

// routes
app.get('/shop', function(req, res) {
  const data = shopsData.getAll()
  sendData(res, data)
})

app.get('/shop/:id', function(req, res) {
  const { id } = req.params
  sendData(res, shopsData.getById(id))
})

app.post('/shop/:id', function(req, res) {
  const { body, params } = req
  const { id } = params
  shopsData.updateById(id, body)
  sendData(res, shopsData.getById(id))
})

app.post('/appointment', function(req, res) {
  const { body, params } = req
  const id = uuid.v4()
  appointmentsData.create(id, {
    ...body,
    confirmed: false,
  })

  sendData(res, appointmentsData.getById(id))
})

app.get('/appointment/:id', function(req, res) {
  const { id } = req.params
  sendData(res, appointmentsData.getById(id))
})

const getApiCaller = key => {
  const network = {
    host: 'testnet1.everitoken.io',
    port: 8888,
    protocol: 'http',
  }

  const apiCaller = EVT({
    endpoint: network,
    keyProvider: [key],
  })
  return apiCaller
}

app.post('/everipass/verify', async function(req, res) {
  console.log('hit')
  const { body, params } = req
  const { link, id, privkey } = body
  const apiCaller = getApiCaller(privkey)
  const appointment = appointmentsData.getById(id)

  const destroyRes = await apiCaller.pushTransaction(
    { maxCharge: 10000 },
    new EVT.EvtAction('everipass', {
      link,
    })
  )

  appointmentsData.updateById(appointment.id, {
    token: {
      ...appointment.token,
      destroyed: true,
      destroyTx: destroyRes.transactionId,
    },
  })

  sendData(res, appointmentsData.getById(appointment.id))
})

app.post('/everipass', async function(req, res) {
  const { body, params } = req
  const { pubkey, privkey, appointment } = body
  const apiCaller = getApiCaller(privkey)

  const linkId = await EVT.EvtLink.getUniqueLinkId()
  EVT.EvtLink.getEVTLinkQrImage(
    'everiPass',
    {
      keyProvider: [privkey],
      domainName: appointment.token.domain,
      tokenName: appointment.token.name,
      autoDestroying: true,
      linkId,
    },
    {
      autoReload: true,
    },
    async (err, linkRes) => {
      if (err) {
        // alert(e.message);
        console.error(err.rawServerError)
        return
      }

      res.end(JSON.stringify(linkRes))
    }
  )
})

app.post('/appointment/:id', async function(req, res) {
  const { body, params } = req
  const { pubkey, privkey, appointment } = body
  const apiCaller = getApiCaller(privkey)
  // get domain
  const domain = `shop-${appointment.shopId}-confirmation`
  try {
    const domainRst = await util.createDomain(apiCaller, domain, pubkey)
  } catch (e) {}
  // issue token
  const tokenName = uuid.v4().slice(0, 21)
  const tokenRst = await util.createToken(
    apiCaller,
    domain,
    [tokenName],
    pubkey
  )

  // transfer token
  const tx = await util.transferToken(
    apiCaller,
    domain,
    tokenName,
    appointment.pubkey,
    'Booking Confirmation'
  )

  appointmentsData.updateById(appointment.id, {
    token: {
      domain,
      name: tokenName,
      tx: tokenRst.transactionId,
      transferTx: tx.transactionId,
    },
    confirmed: true,
  })

  sendData(res, appointmentsData.getById(appointment.id))
})

app.get('/appointment', function(req, res) {
  const { shopId } = req.query
  if (shopId) {
    sendData(res, appointmentsData.getByShopId(shopId))
    return
  }
  sendData(res, appointmentsData.getAll())
})
