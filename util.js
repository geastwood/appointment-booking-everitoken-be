const EVT = require('evtjs')
const createDomain = async (api, domain, key) => {
  return await api.pushTransaction(
    { maxCharge: 10000 },
    new EVT.EvtAction('newdomain', {
      name: domain,
      creator: key,
      issue: {
        name: 'issue',
        threshold: 1,
        authorizers: [
          {
            ref: '[A] ' + key,
            weight: 1,
          },
        ],
      },
      transfer: {
        name: 'transfer',
        threshold: 1,
        authorizers: [
          {
            ref: '[G] .OWNER',
            weight: 1,
          },
        ],
      },
      manage: {
        name: 'manage',
        threshold: 1,
        authorizers: [
          {
            ref: '[A] ' + key,
            weight: 1,
          },
        ],
      },
    })
  )
}

const createToken = async (api, domain, names, key) => {
  return await api.pushTransaction(
    { maxCharge: 10000 },
    new EVT.EvtAction('issuetoken', {
      domain,
      names,
      owner: [key],
    })
  )
}

const transferToken = async (api, domain, name, keys, memo) => {
  return await api.pushTransaction(
    { maxCharge: 10000 },
    new EVT.EvtAction('transfer', {
      domain,
      name,
      to: [keys],
      memo,
    })
  )
}

module.exports = {
  createDomain,
  createToken,
  transferToken,
}
