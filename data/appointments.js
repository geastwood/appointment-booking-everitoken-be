const db = require('./db')

const api = {
  getById: id => {
    return db
      .get('appointments')
      .find({ id: id })
      .value()
  },
  updateById: (id, data) => {
    db
      .get('appointments')
      .find({ id: id })
      .assign({ ...data })
      .write()
  },
  getAll: () => {
    const appointments = db.get('appointments')
    return appointments
  },
  getByShopId: shopId => {
    return db
      .get('appointments')
      .filter({ shopId: Number(shopId) })
      .value()
  },
  create: (id, data) => {
    db
      .get('appointments')
      .push({ id, ...data })
      .write()
  },
}

module.exports = api
