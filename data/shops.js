const db = require('./db')

const api = {
  getById: id => {
    return db
      .get('shops')
      .find({ id: Number(id) })
      .value()
  },
  updateById: (id, data) => {
    db
      .get('shops')
      .find({ id: Number(id) })
      .assign({ ...data })
      .write()
  },
  getAll: () => {
    const shops = db.get('shops')
    return shops
  },
}

module.exports = api
